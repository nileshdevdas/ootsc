
export interface Employee {
    firstName : string;
    lastName? : string; 
    email? : string; 
    age : number; 
    id : number; 
    middleName?: string;    
}
// {
    // what attributes/ properties are inside this ... is determined by the above
// }

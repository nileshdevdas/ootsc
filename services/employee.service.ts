import { Employee } from "../models/employee";
import axios from 'axios';



export class EmployeeService {

    constructor(private employee: Employee) {
        console.log(this.employee.firstName);
        console.log(axios);
    }

    createEmployee() {
        axios.post('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8', this.employee, {}).then((result) => {
            console.log(result);
        }).catch((err) => {
            console.log(err);
        });
    }

    findEmployee() {
        axios.get('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8').then((result) => {
            console.log(result);
        }).catch((err) => {
            console.log(err);
        });
    }

    updateEmployee() {
        // call api 
        axios.put('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8', this.employee, {}).then((result) => {
            console.log(result);
        }).catch((err) => {
            console.log(err);
        });
    }

    deleteEmployee() {
        // call api 
        axios.delete('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8').then((result) => {
            console.log(result);
        }).catch((err) => {
            console.log(err);
        });

    }



}
let employee = {
    id: 1,
    age: 35,
    firstName: 'John Doe'
};
new EmployeeService(employee).createEmployee();


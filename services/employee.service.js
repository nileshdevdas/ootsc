"use strict";
exports.__esModule = true;
exports.EmployeeService = void 0;
var axios_1 = require("axios");
var EmployeeService = /** @class */ (function () {
    function EmployeeService(employee) {
        this.employee = employee;
        console.log(this.employee.firstName);
        console.log(axios_1["default"]);
    }
    EmployeeService.prototype.createEmployee = function () {
        axios_1["default"].post('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8', this.employee, {}).then(function (result) {
            console.log(result);
        })["catch"](function (err) {
            console.log(err);
        });
    };
    EmployeeService.prototype.findEmployee = function () {
        axios_1["default"].get('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8').then(function (result) {
            console.log(result);
        })["catch"](function (err) {
            console.log(err);
        });
    };
    EmployeeService.prototype.updateEmployee = function () {
        // call api 
        axios_1["default"].put('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8', this.employee, {}).then(function (result) {
            console.log(result);
        })["catch"](function (err) {
            console.log(err);
        });
    };
    EmployeeService.prototype.deleteEmployee = function () {
        // call api 
        axios_1["default"]["delete"]('https://run.mocky.io/v3/ff702b86-f503-49c1-ba7c-6f00256013a8').then(function (result) {
            console.log(result);
        })["catch"](function (err) {
            console.log(err);
        });
    };
    return EmployeeService;
}());
exports.EmployeeService = EmployeeService;
var employee = {
    id: 1,
    age: 35,
    firstName: 'John Doe'
};
new EmployeeService(employee).createEmployee();

import { EmployeeService } from "../services/employee.service";

function doTask() {
    let employee = {
        id: 1,
        age: 35,
        firstName: 'John Doe'
    };
    let service = new EmployeeService(employee);
    service.createEmployee();
}

doTask();
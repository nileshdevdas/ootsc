"use strict";
exports.__esModule = true;
var employee_service_1 = require("../services/employee.service");
function doTask() {
    var employee = {
        id: 1,
        age: 35,
        firstName: 'John Doe'
    };
    var service = new employee_service_1.EmployeeService(employee);
    service.createEmployee();
}
doTask();
